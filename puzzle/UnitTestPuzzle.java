package puzzle;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UnitTestPuzzle {


		@DisplayName("UnitTestPuzzle class demonstrating 8 cases of word search in 8 directions from the Grid."
				+ "Words are not matched will results as Not Found")
		@Test
		// Case 1:
		
		void testHorizontalForward() { // Test with extra spaces in word(s)

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("GO O D");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("4:0") && retStr.contains("4:3"));
		}

		@Test
		// Case 2:
		void testHorizontalBackward() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("BYE");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("1:3") && retStr.contains("1:1"));
		}
		
		@Test
		// Case 3:
		void testVerticalForward() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			puzzleWords.add("BLY S"); // test with extras spaces in word(s)
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("3:2") && retStr.contains("0:2"));
		}
		
		@Test
		// Case 4:
		void testVerticalBackward() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			puzzleWords.add("SYLB");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("0:2") && retStr.contains("3:2"));
		}
		
		@Test
		// Case 5:
		void testDiagonalNorthEast() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("GVLB");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("4:0") && retStr.contains("1:3"));
		}
		
		@Test
		// Case 6:
		void testDiagonalSouthWestt() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("DYKC");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("0:3") && retStr.contains("3:0"));
		}
		
		@Test
		// Case 7:
		void testDiagonalNorthhWest() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("OLLE");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("4:4") && retStr.contains("1:1"));
		}
		
		@Test
		// Case 8:
		void testDiagonalSouthEast() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("AYZN");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("0:1") && retStr.contains("3:4"));
		}

		@Test
		// Case 9:
		void testWordNotFoud() {

			List<String> words = new ArrayList<String>();
			int rows = 5;
			int columns = 5;

			// Set<String> puzzleWords = null;
			Set<String> puzzleWords = new HashSet<String>();
			words.add("H A S D F\n");
			words.add("G E Y B H\n");
			words.add("J K L Z X\n");
			words.add("C V B L N\n");
			words.add("G O O D O\n");
			
			puzzleWords.add("XYZ");
			for (String s : puzzleWords) {
				System.out.println(s);
			}
			WordSearchGrid grid = new WordSearchGrid(words, rows, columns);
			WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

			System.out.print(wsp.printResults(wsp.checkForMatches()));
			String retStr = wsp.printResults(wsp.checkForMatches());
			assert (retStr.contains("Not Found"));
		}
		
	}


