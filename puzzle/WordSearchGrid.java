package puzzle;

import java.util.List;

public class WordSearchGrid {

	public WordSearchGrid(List<String> word_rows, int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		String[] tmp = null;		
		charMatrix = new char[rows][cols];
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				String str = word_rows.get(r);
				tmp = str.split(" ");
				charMatrix[r][c] = tmp[c].charAt(0);
			}
		}
	}

	public String to_string() {
		String str = "";
		str += this.rows + "x" + this.cols + "\n";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				str += charMatrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}

	public final char[][] charMatrix; // data grid NxN 
	public final int rows, cols;
}
