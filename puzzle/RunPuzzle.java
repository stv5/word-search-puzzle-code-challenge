package puzzle;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;
import java.io.IOException;
import java.io.FileNotFoundException;

public class RunPuzzle {
    public static void main(String[] args) throws IOException {

        String fileName = "";
        if ( args.length < 1) {
            System.out.println("Usage: java puzzle.RunPuzzle ./<input file name>");
            // Example1: java puzzle.RunPuzzle ./input1.txt
            // Exmplle2: java puzzle/RunPuzzle ./input2.txt
            // Either way should work
            System.exit(1);
        } else {
            fileName = args[0];
        }
        

        FileReader fileReader = null;
        try {
            fileReader = new FileReader(fileName);
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + fileName);
            System.exit(1);
        }

        String line = null;
        WordSearchGrid grid = null;
        Set<String> puzzleWords = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(fileReader);

            // Read the first line from the input file to get the Matrix Dimension (RowsxColums)
            if ((line = br.readLine()) == null) {
                throw new RuntimeException("NO LINES FOUND IN PUZZLE FILE....");
            }
            
            final int rows = Character.getNumericValue(line.charAt(0));
            final int columns = Character.getNumericValue(line.charAt(2));
            List<String> list = new ArrayList<String>();
            for (int i = 0; i< rows; i++) {
                String l = br.readLine();
                list.add(l);
            }

            grid = new WordSearchGrid(list, rows, columns);
            
            puzzleWords = new LinkedHashSet<String>();
            while ((line = br.readLine()) != null) {
                puzzleWords.add(line);
            }
        } catch (IOException e) {
            System.out.println("IO error occurred: " + e);
        } finally {
            br.close();
        }


        WordSearchPuzzle wsp = new WordSearchPuzzle(grid, puzzleWords);

        System.out.print(wsp.printResults(wsp.checkForMatches()));
    }
}