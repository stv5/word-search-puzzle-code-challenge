package puzzle;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.*;


public class WordSearchPuzzle {

/**
	* WordSearchPuzzle class - Locates the list of puzzle words on the grid. As each of the words located,
	* it prints out the word found from <row:column> to <row::column> of each word.
	* 
**/
	public WordSearchPuzzle(WordSearchGrid grid, Set<String> words) {
		this.grid = grid;
		this.words = words;
		// remove all spaces from puzzle words but keep the original words as they are in the input file
		// for the purpose of printing out the results 
		Set<String> tmp = new HashSet<String>();  
		for ( String str : this.words) {
			linkHashMap.put(str.replaceAll("\\s", ""), str); //keep original words and words with no spaces
			tmp.add(str.replaceAll("\\s", ""));
		}
		this.words = tmp;
	}

	/**
	 * Check for East/West/Up/Down/Diagnals (up/down) - 8 directions return number
	 * of matches
	 */
	public Map<String, String>  checkForMatches() {
		Map<String, String> solution = new HashMap<String, String>(); // Save results in String format

		for (int r = 0; r < grid.rows; r++)
			for (int c = 0; c < grid.cols; c++)
				for (int rd = -1; rd <= 1; rd++)
					for (int cd = -1; cd <= 1; cd++)
						if (rd != 0 || cd != 0)
							solveDirection(r, c, rd, cd, solution);
		return solution;
	}

	/**
	 * Search the grid from a starting point and direction. return number of matches
	 */
	private void solveDirection(int baseRow, int baseCol, int rowDelta, int colDelta, Map<String, String>  solution) {
		
		String charSequence = "";

		charSequence += grid.charMatrix[baseRow][baseCol];

		for (int i = baseRow + rowDelta, j = baseCol + colDelta; i >= 0 && j >= 0 && i < grid.rows
				&& j < grid.cols; i += rowDelta, j += colDelta) {
			charSequence += grid.charMatrix[i][j];

			if (words.contains(charSequence)) {
				String result = " at " + baseRow + ":" + baseCol + " to " + i + ":" + j;
				solution.put(charSequence, result);
			}
		}
	}


	/**
	* show results of puzzle words in order as they are in the input file
	*/
	public String printResults(Map<String, String>  solution) {
		String ret = "";
		for (String str : words) {
			if (solution.containsKey(str)) {
				ret += linkHashMap.get(str) + solution.get(str);
			} else if (str.length() != 0 ) {
				ret += str +" Not Found";
			}
			ret += "\n";
		}
		return ret;
	}

	
	private WordSearchGrid grid;
	private Set<String> words;
	private LinkedHashMap<String, String> linkHashMap
    		= new LinkedHashMap<String, String>();
}

